package WebService::Toodledo::Context;
use Modern::Perl '2015';
use Carp;
use Moo;
use namespace::clean;

has id => (is => 'ro', required => 1);
has name => (is => 'rw', required => 1);
has private => (is => 'rw', default => sub { '0' } );

1;