package WebService::Toodledo::Auth;
use Modern::Perl '2015';
use Carp;
use Moo;
use Config::Tiny;
use LWP::Authen::OAuth2;
use JSON qw(decode_json encode_json);
use JSON::Parse 'valid_json';
use String::Random qw/random_string/;
use feature 'signatures';
no warnings 'experimental::signatures';
use namespace::clean;
 
# ABSTRACT: A Toodledo Authentication Object; handles low-level calls to Toodledo API
 
has 'api_base'      => (is => 'ro', default => sub { 'https://api.toodledo.com/3' });
has 'config_file'   => ( is => 'ro', default  => sub { "$ENV{HOME}/.toodledorc" } );
has 'config'        => ( is => 'rw', lazy => 1, builder => 1 );
has 'scope'         => ( is => 'ro', lazy => 1, default  => sub { 'basic tasks' }  );
has 'auth'          => ( is => 'rw', lazy => 1, builder => 1, handles => [ qw( get post ) ] );
 
sub setup( $self ) {
  # Request Client details if non existent
  if (! $self->config->{auth}{client_id} ) {
    $self->config->{auth}{client_id} = $self->prompt("Paste enter your client_id");
  }
   
  if (! $self->config->{auth}{client_secret} ) {
    $self->config->{auth}{client_secret} = $self->prompt("Paste enter your client_secret");
  }
   
  # Build auth object - TODO: Write a toodledo authentication provider! Issue #1
  my $oauth2 = LWP::Authen::OAuth2->new(
    client_id => $self->{config}{auth}{client_id},
    client_secret => $self->{config}{auth}{client_secret},
   # service_provider => "Toodledo",
    redirect_uri => "http://127.0.0.1",
    scope => $self->{scope},
    authorization_endpoint => 'https://api.toodledo.com/3/account/authorize.php',
    token_endpoint => 'https://api.toodledo.com/3/account/token.php',
  );
 
  # Get authentican token string
  say "Log into the Toodledo account and browse the following url";
  my $url = $oauth2->authorization_url( state => random_string('........') );
  say $url;
  my $code = $self->prompt("Paste code result here");
  $oauth2->request_tokens(code => $code);
  $self->config->{auth}{token_string} = $oauth2->token_string;
  $self->config->write($self->{config_file});
}
 
sub _build_config( $self ) {
  my $config;
  if ( -e $self->{config_file} ) {
    $config = Config::Tiny->read( $self->{config_file} );
    unless ($config->{auth}{client_id} 
            && $config->{auth}{client_secret}) {
      die <<"END_DIE";
Cannot find user credentials in $self->{config_file}
 
You'll need to have a $self->{config_file} file that looks like 
the following:
 
    [auth]
    client_id     = xxxxx
    client_secret = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 
You can get these values by going to https://www.toodledo.com/settings/api
 
Running 'toodledo --setup' or \$toodledo->auth->setup will run you through
setting up Oauth2.
 
END_DIE
}
  } else {
    $config = Config::Tiny->new();
  }
  return $config;
}
 
sub _build_auth( $self) {
  $self->config;
  croak "Missing token, please re-run setup" unless $self->config->{auth}{token_string};
 
  my $oauth2 = LWP::Authen::OAuth2->new(
    client_id => $self->{config}{auth}{client_id},
    client_secret => $self->{config}{auth}{client_secret},
    #service_provider => "Toodledo",   # TODO release a seperate service provider!
    token_string => $self->config->{auth}{token_string},
    authorization_endpoint => 'https://api.toodledo.com/3/account/authorize.php',
    token_endpoint => 'https://api.toodledo.com/3/account/token.php',
  );
  return $oauth2;
}

sub can_refresh_tokens($self) {
   return $self->auth->can_refresh_tokens;
}

sub should_refresh_tokens($self) {
  return $self->auth->should_refresh;
}

sub get_api($self, $api_path, $params = undef) {
  use Data::Dumper; warn Dumper($params);
  my $url = $self->{api_base}.$api_path;
  $url .= '?' if ($params);
  foreach my $key (keys %$params) {
    $url .= "\&$key=".$params->{$key};
  }
  say $url;
  my $response = $self->auth->get($url);
  my $json = $response->decoded_content;
  if (! valid_json($json) ) {
    croak("Something went wrong, a JSON string wasn't returned\n$json");
  }
  if ($ENV{STRAVA_DEBUG}) {
    say Dumper($json);
  }
  return decode_json($json);
}

sub prompt($self, $question, $default) { # inspired from here: http://alvinalexander.com/perl/edu/articles/pl010005
  if ($default) {
    say $question, "[", $default, "]: ";
  } else {
    say $question, ": ";
    $default = "";
  }
 
  $| = 1;               # flush
  $_ = <STDIN>;         # get input
 
  chomp;
  if ("$default") {
    return $_ ? $_ : $default;    # return $_ if it has a value
  } else {
    return $_;
  }
}

 1;

 __END__