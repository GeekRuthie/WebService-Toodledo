package WebService::Toodledo;
use Modern::Perl '2015';
# VERSION
# ABSTRACT
use Carp;
use autodie;
use Moo;
use namespace::clean;
use feature 'signatures';
no warnings 'experimental::signatures';

use WebService::Toodledo::Auth;
use WebService::Toodledo::Context;
use WebService::Toodledo::Folder;

### Fields

has 'auth' => (
  is => 'ro',
  isa => sub { "WebService::Toodledo::Auth" },
  lazy => 1,
  builder => 1,
);

has 'scope' => (
  is => 'ro',
  default => sub { 'basic tasks' },
);

### Methods

sub get ($self, $what){
  my %classes = (
    'contexts' => 'Context',
    'folders' => 'Folder',
    'tasks' => 'Task',
  );
  return undef if !$classes{$what};
  my @objects;
  my $raw_objects = $self->auth->get_api("/$what/get.php");
  my $class = 'WebService::Toodledo::'.$classes{$what};
  foreach my $obj (@$raw_objects) {
   # use Data::Dumper; warn Dumper($obj);
     push @objects, $class->new( $obj );
  }
  return \@objects;
}

sub _build_auth( $self ) {
  return WebService::Toodledo::Auth->new( scope => $self->scope );
}

1;

__END__

# ABSTRACT:

=pod

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 SUBROUTINES/METHODS

=head1 ATTRIBUTES

=head1 DIAGNOSTICS

=head1 CONFIGURATION AND ENVIRONMENT

=head1 DEPENDENCIES

=head1 INCOMPATIBILITIES

=head1 BUGS AND LIMITATIONS

=head1 ACKNOWLEDGEMENTS

=head1 SEE ALSO

=cut
